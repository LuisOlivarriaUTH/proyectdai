package uth.luisolivarria.proyectidgs.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import uth.luisolivarria.proyectidgs.domain.GetPokemon
import uth.luisolivarria.proyectidgs.models.Pokemon

class PokemonViewModel : ViewModel() {
    val pokemonModel = MutableLiveData<Pokemon>()
    val getPokemon = GetPokemon()
    fun onCreate(){
        viewModelScope.launch {
            val result = getPokemon()
        }
    }
}