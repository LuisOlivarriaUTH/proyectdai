package uth.luisolivarria.proyectidgs.network

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import okhttp3.Dispatcher
import uth.luisolivarria.proyectidgs.core.RetrofitClient
import uth.luisolivarria.proyectidgs.models.Pokemon

class PokemonService {

    private val retrofit = RetrofitClient.getRetrofit()

    suspend fun getPokemon(): List<Pokemon>{
        return  withContext(Dispatchers.IO){
            val response = retrofit.create(PokeAPIClient::class.java).getAllPokemon()
            response.body() ?: emptyList()
        }
    }
}