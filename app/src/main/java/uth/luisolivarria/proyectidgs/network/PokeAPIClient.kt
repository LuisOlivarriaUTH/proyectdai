package uth.luisolivarria.proyectidgs.network
import retrofit2.Response
import retrofit2.http.GET
import uth.luisolivarria.proyectidgs.models.Pokemon

interface PokeAPIClient {
 @GET("pokemon")
    suspend fun getAllPokemon(): Response<List<Pokemon>>
}