package uth.luisolivarria.proyectidgs

import android.os.Bundle
import android.os.Message
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import okhttp3.Call
import okhttp3.Response
import uth.luisolivarria.proyectidgs.adapter.PokemonAdapter
import uth.luisolivarria.proyectidgs.databinding.FragmentDashboardBinding
import uth.luisolivarria.proyectidgs.models.Pokemon
import uth.luisolivarria.proyectidgs.network.PokemonService



class DashboardFragment : Fragment() {

    private lateinit var adapter: PokemonAdapter
    private lateinit var pokeList: List<Pokemon>
    private var _binding : FragmentDashboardBinding? = null
    private lateinit var pokeService: PokemonService
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        getPokemonData()
        setupRecycler()
        return binding.root
    }

    suspend fun getPokemonData() {
        val call = pokeService.getPokemon()
        call.enqueue(object : Callback<List<Pokemon>>){
            fun onResponse(call: Call<List<Pokemon>>?, response: Response<List<Pokemon>>){
                pokeList = response.body()!!
            }

            fun onFailure(call: Call<Message?>, L: Throwable){
                Toast.makeText(context, "Error "+L.message,Toast.LENGTH_SHORT)
            }
        }
    }

    fun setupRecycler(){
        //val lManagerList: RecyclerView.LayoutManager = LinearLayoutManager(context)
        val lManager2Table: RecyclerView.LayoutManager = GridLayoutManager(context,  3)
        binding.pokemonRecycler.setLayoutManager(lManager2Table)
        binding.pokemonRecycler.adapter = adapter
    }

}