package uth.luisolivarria.proyectidgs.data

import uth.luisolivarria.proyectidgs.models.Pokemon
import uth.luisolivarria.proyectidgs.models.PokemonProvider
import uth.luisolivarria.proyectidgs.network.PokemonService

class PokemonData {
    private val api = PokemonService()

    suspend fun getAllPokemon(): List<Pokemon>{
        val response = api.getPokemon()
        PokemonProvider.pokemon = response
        return response
    }
}