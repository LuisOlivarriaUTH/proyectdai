package uth.luisolivarria.proyectidgs.domain

import uth.luisolivarria.proyectidgs.data.PokemonData

class GetPokemon {
    private val data = PokemonData()

    suspend operator fun invoke() = data.getAllPokemon()
}